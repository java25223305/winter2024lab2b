import java.util.Scanner;

public class GameLauncher {
    public static Scanner reader = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Hello, welcome to the game factory!");
        System.out.print("Would you like to play Hangman (Type 1), or Wordle (Type 2): ");
        int userChoice = reader.nextInt();
        while (userChoice != 1 && userChoice !=2) {
            System.out.print("Enter a valid number please: ");
            userChoice = reader.nextInt();
        }

        switch (userChoice) {
            case 1:
                System.out.println("Welcome to HangMan!");
                String word = reader.nextLine();
                int wordLength = word.length();

                //Loop to ask for word with four different characters
                while (wordLength != 4 || !(WordGuesser.hasUniqueCharacters(word))){

                    if (wordLength !=4)
                        System.out.println("Your word to guess contains " + wordLength + " characters. It must contain 4.");
                        
                    else 
                    if(!(WordGuesser.hasUniqueCharacters(word))) 
                        System.out.println("The word must contain four different characters");
                    
                    System.out.print("Enter word: ");
                        
                        word = reader.nextLine().toLowerCase();
                        wordLength = word.length();
                }
                System.out.println("\033[H\033[2J"); //Erase everything that is on the console to make sure prompted word is hidden

                WordGuesser.runGame(word);
                break;

            case 2:
                String targetWord = Wordle.wordGenerator();
                System.out.println("Welcome to Wordle!");
                Wordle.runGame(targetWord);;
                break;
        }
    }
}
