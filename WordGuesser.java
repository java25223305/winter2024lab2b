import java.util.Scanner;

public class WordGuesser {

    //Takes word and char as entry
    //Returns position of the char in the word
    //Returns -1 if char not in word
    public static int isLetterInWord(String word, char c){
 
       int position = 0;
       final int wordLength = 4;
       //Loop to check position of char in word
       while (position < wordLength) {
            if (c == word.charAt(position++)){
            return position;
            }
       }
       return -1;
    }

    //Takes word as entry, 4 boolean for if each letter of word is displayer or not
    //Prints word with either revealed or hidden letters 
    public static void printWord(String word, boolean letter1, boolean letter2, boolean letter3, boolean letter4) { 

        String newWord = "";
        
        if (!letter1)
            newWord = newWord +  "_ ";
        else
            newWord = newWord + word.charAt(0); 
        
        if (!letter2)
            newWord = newWord +  "_ ";
        else
            newWord = newWord + word.charAt(1); 
        
        if (!letter3)
             newWord = newWord +  "_ ";
        else
            newWord = newWord + word.charAt(2); 
        
        if (!letter4)
            newWord = newWord +  "_ ";
        else
            newWord = newWord + word.charAt(3); 
            
        System.out.println("Your result is " + newWord);
    }

    //Takes four-character word as entry
    //Asks user to guess character of the word one at a time
    //Prints word with either revealed or hidden letters each time a char is guessed
    public static void runGame(String word) {
        Scanner reader = new Scanner(System.in);

        boolean letter1 = false;
        boolean letter2 = false;
        boolean letter3 = false;
        boolean letter4 = false;
        int timesMissed = 0;


        //Loop to ask for char whilst word not fully revealed or if > 6 misses
        while (!(letter1 && letter2 && letter3 && letter4) && timesMissed < 6 ){

            System.out.println("Guess a letter from the word: ");
            String wordGuessed = reader.nextLine().toLowerCase();
            char charGuessed = wordGuessed.charAt(0); 
            
            if (isLetterInWord(word, charGuessed) == -1) {
                System.out.printf("\"%c\" is not in the word, you have " + (6 - (++timesMissed)) + " attempts left \n", charGuessed);
            }
            if (isLetterInWord(word, charGuessed) == 1)
                letter1 = true;
                
            if (isLetterInWord(word, charGuessed) == 2)
                letter2 = true;
                
            if (isLetterInWord(word, charGuessed) == 3)
                letter3 = true;
                
            if (isLetterInWord(word, charGuessed) == 4)
                letter4 = true;
        
            printWord(word, letter1, letter2, letter3, letter4);

        }
            //Print win/lose messages
            if ((letter1 && letter2 && letter3 && letter4))
                System.out.println("You won!");

            else {
                System.out.println("\033[H\033[2J"); //Erase everything that is on the console
                System.out.println("You lost, the word was: " + word);
            }
            reader.close();
    }
    
    //Takes four-character word as entry
    //Returns true if all characters are different
    public static boolean hasUniqueCharacters (String word) {
        if (word.charAt(0)!= word.charAt(1) 
           && word.charAt(0)!= word.charAt(1)
           && word.charAt(0)!= word.charAt(2)
           && word.charAt(0)!= word.charAt(3)
           && word.charAt(1)!= word.charAt(2)
           && word.charAt(1)!= word.charAt(3)
           && word.charAt(2)!= word.charAt(3))
           return true;

        return false;
    }
}