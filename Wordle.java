import java.util.Random;
import java.util.Scanner;
public class Wordle {
    //Declare Scanner and Constants
    public static Scanner reader = new Scanner(System.in);
    public static final int wordLength = 5;
    public static final int maxAttempts = 6;

    //No entry parameter
    //Returns random word from array of words
    public static String wordGenerator() {

        Random randomizer = new Random();
        //Create array of 20 5-different-letter words
        String[] words = {
            "TIGER", "BLEND", "SNACK", "TOUCH", "GRASP",
            "BIRTH", "QUICK", "CLOUD", "FLUTE", "LIONS",
            "PLAID", "SWORD", "PRISM", "TABLE", "CROWN",
            "GLAZE", "PUNCH", "IDOLS", "HOUSE", "COUNT"
        };
        //Pick a random word using random index number
        String randomWord = words[randomizer.nextInt(words.length)];
        return randomWord;
    }

    //Takes word and character as entry
    //Returns true if character is in the word
    public static boolean letterInWord(String word, char letter) {

        boolean isLetterInWord = false;

        //Loop to verify if the character is in the word
        for (int index = 0; index < word.length(); index++) {
            if (letter == word.charAt(index))
                isLetterInWord = true;
        }
        return isLetterInWord;
    }

    //Takes word, character and number as entry
    //Returns true if character is in the word & correct spot
    public static boolean letterInSlot(String word, char letter, int position) {

        boolean correctPosition = false;

        //Loop to verify if letter is in correct word slot
        for (int index = 0; index < word.length(); index++) {
            if (letter == word.charAt(index) && index == position){
                correctPosition = true;
                return correctPosition;
            }
        }
        return correctPosition;
    }

    //Takes two strings as entry, an answer and a guess
    //Creates and returns array of colors representing if the letters of the guess are in the correct spot. See inline comments below
    public static String[] guessWord(String answer, String guess) {

        String[] colors = new String[answer.length()];

        //loop to determine color of each letter of guessed word
        for (int index = 0; index < answer.length(); index++) {
            //Green = Letter in correct slot
            if (letterInSlot(answer, guess.charAt(index), index)) {
                colors[index] = "green";
            }
            else 
            //Yellow = Letter in word but incorrect spot
            if (letterInWord(answer, guess.charAt(index))) {
                colors[index] = "yellow";
            }
            //White = Letter not in word
            else {
                colors[index] = "white";
            }
        }
        return colors;
    }

    //Takes word and array of colors from guessWord
    //Prints each character in the appropriate color
    public static void presentResults(String word, String[] colors) {

        //Loop to print each letter in corresponding color
        for (int index = 0; index < colors.length; index++) {
            //Print char in green
            if (colors[index].equals("green")) {
                System.out.print("\u001B[32m" + word.charAt(index));
            }
            else
            //Print char in yellow
            if (colors[index].equals("yellow")) {
                System.out.print("\u001B[33m" + word.charAt(index));
            }
            //Print char in white
            else {
                System.out.print("\u001B[0m" + word.charAt(index));
            }
        }
        System.out.println("\u001B[0m");
    }

    //Asks the user to input 5-letter words and returns it to upper case
    public static String readGuess() {
        
        System.out.println("Enter your guess: ");
        String word = reader.nextLine();
        
        while (word.length() != wordLength) {
            /*Just did this as practice:
            System.out.printf("\"%s\" contains %d letters, the word must contain five \n", word.toLowerCase(), word.length()); */
            System.out.println("Word must contain " + wordLength + " letters.");
            System.out.println("Enter a new guess: ");
            word = reader.nextLine();
        }
        return word.toUpperCase();
    }

    public static void runGame(String targetWord) {

        String guess = readGuess();
        int tries = 1;

        //Loop to keep the user guessing unless word is found or 6 attemps exceeded
        while (!(guess.equals(targetWord)) && tries < maxAttempts) {
            String[] colors = guessWord(targetWord, guess);
            presentResults(guess, colors);
            System.out.println(maxAttempts-tries + " attempts left");
            tries++;
            guess = readGuess();
        }
        //Display final messages
        if (guess.equals(targetWord))
            System.out.println("Congrats, You win! ");
        else 
            System.out.println("You lost. the word was: " + targetWord.toLowerCase());
    }
}